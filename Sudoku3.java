import java.util.*;

public class Sudoku3 {

public int N=9;
public int num_nodes_visited=0;

public static void main (String args[]) {
   new Sudoku3().go();
}


public void go() {  
   int[][] sudoku = new int[N][N];
   Scanner in = new Scanner(System.in);
   for (int i=0; i<N; ++i) {
      for (int j=0; j<N; ++j) {
         sudoku[i][j] = in.nextInt();
         //must enter 0s for blank spaces in input sudoku
      }
   }
   if (backtrack_solve(sudoku)) {
      System.out.println("Number of nodes visited: " + num_nodes_visited);
      print_sudoku(sudoku); //print solution
   } else {
      System.out.println("Could not find a solution.");
      System.out.println("The configuration of the input sudoku must have been illegal.");
   }  
}

public boolean backtrack_solve(int[][] sudoku) {
   if (is_solved(sudoku)) {
      return true;
   }
   if (!forward_check(sudoku)) {
      return false;
   }
   ++num_nodes_visited;
   
   ArrayList<Integer> most_constrained = find_most_constrained(sudoku);
   int i = most_constrained.get(0);
   int j = most_constrained.get(1);
   
   int lc_val = find_least_constraining(sudoku, i, j);
   if (is_legal(sudoku, i, j, lc_val)) {
      sudoku[i][j] = lc_val;
      if (backtrack_solve(sudoku)) {
         return true;
      }
      sudoku[i][j] = 0;
   }

   for (int val=1; val<=N; ++val) {
      if (is_legal(sudoku, i, j, val)) {
         sudoku[i][j] = val;
         if (backtrack_solve(sudoku)) {
            return true;
         }
         //else, i.e. can't solve sudoku with sudoku[i][j] set to val,
         //increments val and tries again in next iteration of loop
      }
      //else, i.e. this val is not a legal value for sudoku[i][j],
      //increments val and tries again in next iteration of loop
   }
   sudoku[i][j]=0; //need to reset when we backtrack
   return false;
}

public ArrayList<Integer> find_most_constrained(int[][] sudoku) {
   int least_legal_values = 9;
   ArrayList<Integer> most_constrained = new ArrayList<Integer>();   
   for (int I=0; I<N; ++I) {
      for (int J=0; J<N; ++J) {
         if (sudoku[I][J] != 0) continue;
         int temp = find_num_legal_values(sudoku, I, J);
         if (temp<least_legal_values) {
            least_legal_values = temp;
            most_constrained.clear();
            most_constrained.add(I);
            most_constrained.add(J);
         } else if (temp == least_legal_values) {
            most_constrained.add(I);
            most_constrained.add(J);
         }
      }
   }
   if (most_constrained.size()>2) { //use most_constraining heuristic to break ties on most constrained
      ArrayList<Integer> most_constraining = find_most_constraining(sudoku, most_constrained);
      return most_constraining;
   }
   return most_constrained;
}

public int find_num_legal_values(int[][] sudoku, int i, int j) {
   int ret = 0;
   if (sudoku[i][j] != 0) return 1;   
   for (int val=1; val<=N; ++val) {
      if (is_legal(sudoku, i, j, val)) ++ret;
   }
   return ret;
}

public ArrayList<Integer> find_most_constraining(int[][] sudoku, ArrayList<Integer> most_constrained) {
   int most_constraining_num = 0;
   ArrayList<Integer> most_constraining = new ArrayList<Integer>();
   while(!most_constrained.isEmpty()) {
      int i = most_constrained.remove(0);
      int j = most_constrained.remove(0);
      int num_constraints = find_num_constraints(sudoku, i, j);
      if (num_constraints>most_constraining_num) {
         most_constraining.clear();
         most_constraining.add(i);
         most_constraining.add(j);      
      }
   }
   return most_constraining;
}

public int find_num_constraints(int[][] sudoku, int i, int j) {
   int num_constraints=0;
   for (int I=0; I<N; ++I) {
      if (sudoku[I][j] == 0) ++num_constraints;
   }
   for (int J=0; J<N; ++J) {
      if (sudoku[i][J] == 0) ++num_constraints;
   }
   int iBox = i/3;
   int jBox = j/3;
   for (int m=(iBox*3); m<((iBox*3)+3); ++m) {
      for (int n=(jBox*3); n<((jBox*3)+3); ++n) {
         if (sudoku[m][n] == 0) ++num_constraints;
      }
   }
   return num_constraints;
}

public int find_least_constraining(int[][] sudoku, int i, int j) {
   int most_values_allowed = 0;
   int lc_value = 0;
   for (int val=1; val<=N; ++val) {
      int values_allowed = 0;      
      sudoku[i][j] = val;
      for (int I=0; I<N; ++I) {
         values_allowed += find_num_legal_values(sudoku, I, j);
      }
      for (int J=0; J<N; ++J) {
         values_allowed += find_num_legal_values(sudoku, i, J);
      }
      int iBox = i/3;
      int jBox = j/3;
      for (int m=(iBox*3); m<((iBox*3)+3); ++m) {
         for (int n=(jBox*3); n<((jBox*3)+3); ++n) {
            values_allowed += find_num_legal_values(sudoku, m, n);
         }
      }
      if (values_allowed>most_values_allowed) {
         most_values_allowed = values_allowed;
         lc_value = val;
      }
   }
   sudoku[i][j]=0;
   return lc_value;
}

public boolean forward_check(int[][] sudoku) {
   for (int I=0; I<N; ++I) {
      for (int J=0; J<N; ++J) {
         if (sudoku[I][J] != 0) continue;
         boolean has_legal_value = false;
         for (int val=1; val<=N; ++val) {
            if (is_legal(sudoku, I, J, val)) has_legal_value = true;
         }
         if (!has_legal_value) return false;
      }
   }
   return true;
}

public boolean is_solved(int[][] sudoku) {
   for (int i=0; i<N; ++i) {
      for (int j=0; j<N; ++j) {
         if (sudoku[i][j]==0) return false;
      }
   }
   return true;
}

public boolean is_legal(int[][] sudoku, int i, int j, int val) {
   for (int I=0; I<N; ++I) {
      if (sudoku[I][j] == val) return false;
   }
   for (int J=0; J<N; ++J) {
      if (sudoku[i][J] == val) return false;
   }
   int iBox = i/3;
   int jBox = j/3;
   for (int m=(iBox*3); m<((iBox*3)+3); ++m) {
      for (int n=(jBox*3); n<((jBox*3)+3); ++n) {
         if (sudoku[m][n] == val) return false;
      }
   }
   return true;
}

public void print_sudoku(int[][] sudoku) {
   for (int i=0; i<N; ++i) {
      for (int j=0; j<N; ++j) {
         System.out.print(sudoku[i][j] + " ");
      }
      System.out.println();
   }
}

}//Sudoku3
