import java.util.*;

public class Sudoku2 {
   
public int N=9;
public int num_nodes_visited=0;

public static void main (String args[]) {
   new Sudoku2().go();
}

public void go() {  
   int[][] sudoku = new int[N][N];
   Scanner in = new Scanner(System.in);
   for (int i=0; i<N; ++i) {
      for (int j=0; j<N; ++j) {
         sudoku[i][j] = in.nextInt();
         //must enter 0s for blank spaces in input sudoku
      }
   }
   if (backtrack_solve(sudoku, 0, 0)) {
      System.out.println("Number of nodes visited: " + num_nodes_visited);
      print_sudoku(sudoku); //print solution
   } else {
      System.out.println("Could not find a solution.");
      System.out.println("The configuration of the input sudoku must have been illegal.");
   }  
}

public boolean backtrack_solve(int[][] sudoku, int i, int j) {
   if (i>=N) {
      i=0;
      ++j;
   }
   if (j>=N) {
      return true;
   }
   if (sudoku[i][j] != 0) {
      return backtrack_solve(sudoku, i+1, j);
   }
   if (!forward_check(sudoku)) {
      sudoku[i][j]=0;
      return false;
   }
   num_nodes_visited++;
   for (int val=1; val<=N; ++val) {
      if (is_legal(sudoku, i, j, val)) {
         sudoku[i][j] = val;
         if (backtrack_solve(sudoku, i+1, j)) {
            return true;
         }
         //else, i.e. can't solve sudoku with sudoku[i][j] set to val,
         //increments val and tries again in next iteration of loop
      }
      //else, i.e. this val is not a legal value for sudoku[i][j],
      //increments val and tries again in next iteration of loop
   }
   sudoku[i][j]=0; //need to reset when we backtrack
   return false;
}

public boolean forward_check(int[][] sudoku) {
   for (int I=0; I<N; ++I) {
      for (int J=0; J<N; ++J) {
         if (sudoku[I][J] != 0) continue;
         boolean has_legal_value = false;
         for (int val=1; val<=N; ++val) {
            if (is_legal(sudoku, I, J, val)) has_legal_value = true;
         }
         if (!has_legal_value) return false;
      }
   }
   return true;
}

public boolean is_legal(int[][] sudoku, int i, int j, int val) {
   for (int I=0; I<N; ++I) {
      if (sudoku[I][j] == val) return false;
   }
   for (int J=0; J<N; ++J) {
      if (sudoku[i][J] == val) return false;
   }
   int iBox = i/3;
   int jBox = j/3;
   for (int m=(iBox*3); m<((iBox*3)+3); ++m) {
      for (int n=(jBox*3); n<((jBox*3)+3); ++n) {
         if (sudoku[m][n] == val) return false;
      }
   }
   return true;
}

public void print_sudoku(int[][] sudoku) {
   for (int i=0; i<N; ++i) {
      for (int j=0; j<N; ++j) {
         System.out.print(sudoku[i][j] + " ");
      }
      System.out.println();
   }
}

}//Sudoku2
